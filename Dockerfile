FROM maven:3.8.3-openjdk-17
RUN mkdir -p /root/inditex
WORKDIR /root/inditex
COPY ./src /root/inditex/src
COPY ./.mvn /root/inditex
COPY ./pom.xml /root/inditex
COPY ./mvnw /root/inditex
COPY ./mvnw.cmd /root/inditex

RUN echo "about to build"
RUN mvn clean package
RUN echo "finished to build"

COPY target/challenge-1.0.0.jar challenge-1.0.0.jar
ENTRYPOINT ["java","-jar","challenge-1.0.0.jar"]