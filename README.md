# Challenge Inditex

## Technologies

Java 17

Apache Maven 3.x

Spring Boot 3

## Overview

The API has an endpoint to retrieve prices for a specific date, brand, and product. Each price has a priority, so if two
prices match for the same date, the one with higher priority is returned, and in the case where both have the same
priority, an exception is thrown.

This is a Java / Maven / Spring Boot application that serves as a prices query microservice.

## How to Run
This application is packaged as a jar file that has Tomcat embedded. No Tomcat or JBoss installation is necessary. You
run it using the java -jar command.

1. Clone this repository
2. Make sure you are using JDK 1.17 and Maven 3.x
3. You can build the project and run the tests by running ```mvn clean package```
4. Once successfully built, you can run the service by one of these two methods:

```This application is packaged as a jar file that has Tomcat embedded. No Tomcat or JBoss installation is necessary. You run it using the java -jar command.
        java -jar target/challenge-1.0.0.jar
or
        mvn spring-boot:run
```

### With Docker

```
1. docker build --tag=challenge:latest .

2. docker run -p8080:8080 challenge:latest
```
## About the Service

The service is just a simple prices REST service. It uses an in-memory database (H2) to store the data.

The database is populated for startup-data.sql with the following rows:
```
INSERT INTO brand (name)
VALUES('ZARA');

INSERT INTO price (brand_id, start_date, end_date, price_list, product_id, priority, price, currency)
VALUES ( 1, '2020-06-14 00.00.00Z', '2020-12-31 23.59.59Z', 1, 35455, 0, 35.50, 'EUR');
INSERT INTO price (brand_id, start_date, end_date, price_list, product_id, priority, price, currency)
VALUES ( 1, '2020-06-14 15.00.00Z', '2020-06-14 18.30.00Z', 2, 35455, 1, 25.45, 'EUR');
INSERT INTO price (brand_id, start_date, end_date, price_list, product_id, priority, price, currency)
VALUES ( 1, '2020-06-15 00.00.00Z', '2020-06-15 11.00.00Z', 3, 35455, 1, 30.50, 'EUR');
INSERT INTO price (brand_id, start_date, end_date, price_list, product_id, priority, price, currency)
VALUES ( 1, '2020-06-15 16.00.00Z', '2020-12-31 23.59.59Z', 4, 35455, 1, 38.95, 'EUR');
```

In test add these values are for simulate to many prices for the request

```
INSERT INTO price (brand_id, start_date, end_date, price_list, product_id, priority, price, currency)
VALUES ( 1, '2021-06-15 16.00.00Z', '2021-12-10 23.59.59Z', 4, 35456, 1, 40, 'EUR');
INSERT INTO price (brand_id, start_date, end_date, price_list, product_id, priority, price, currency)
VALUES ( 1, '2021-06-15 16.00.00Z', '2021-12-31 23.59.59Z', 4, 35456, 1, 40, 'EUR');
```

**Important: All dates are stored in UTC and the expected format when representing it as text is ISO 8601**

### Find a price example:

```
GET api/v1/prices?applicationDate=2020-06-14T10:00:00Z&productId=35455&brandId=1

Response: HTTP 200
Content:
{
    "id": 1,
    "product_id": 35455,
    "brand_id": 1,
    "start_date": "2020-06-14T00:00:00Z",
    "end_date": "2020-12-31T23:59:59Z",
    "price": 35.5,
    "price_list": 1
}
```

### Cache

It has a basic Spring local cache implemented for 10 seconds to avoid always accessing the database. The time can be
modified from the application.yml in the 'caching' property:

```
caching:
    prices-ttl: 10000
```

### Logger

It has a specific pattern to make the log more understandable, and a requestId has been added to trace the request's
path.

```
13-09-2023 12:21:07.979 [http-nio-8080-exec-1] INFO  requestId:e4a4c71a-a339-422d-8617-99ac30dcf4b7 PriceController.java -> findByFilters(30) - start params PriceRequestDto[applicationDate=2020-06-14T10:00:00Z, productId=35455, brandId=1]
13-09-2023 12:21:07.983 [http-nio-8080-exec-1] INFO  requestId:e4a4c71a-a339-422d-8617-99ac30dcf4b7 PriceServiceImpl.java -> findPriceByFilter(29) - start with params: PriceRequestDto[applicationDate=2020-06-14T10:00:00Z, productId=35455, brandId=1]
13-09-2023 12:21:07.984 [http-nio-8080-exec-1] INFO  requestId:e4a4c71a-a339-422d-8617-99ac30dcf4b7 FindPriceUseCaseImpl.java -> findPriceByFilter(25) - start params date: 2020-06-14T10:00:00Z, productId: 35455, brandId: 1
```

### Error Handler

An exception handler has been implemented for both expected and unexpected exceptions

```

GET api/v1/prices?applicationDate=2020-06-14T10:00:00Z&productId=35455&brandId=
Response: HTTP 400
Content:
{
  "code": 1000,
  "description": [
    "brandId is null"
  ],
  "status": 400
}
```

## Swagger

To view the REST API's documentation, the project has Swagger

http://localhost:8080/swagger-ui/index.html

## H2 console

In the dev profile, the H2 console has been enabled for reviewing the data

http://localhost:8080/h2-console