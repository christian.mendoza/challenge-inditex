package com.inditex.challenge.application.dto;

import java.util.List;

public record ApiErrorDto(Integer code, List<String> description, Integer status) {
}
