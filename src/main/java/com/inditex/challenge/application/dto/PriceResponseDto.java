package com.inditex.challenge.application.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.math.BigDecimal;
import java.time.Instant;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public record PriceResponseDto(Long id, Long productId, Long brandId, Instant startDate, Instant endDate,
                               BigDecimal price, Integer priceList) {
}
