package com.inditex.challenge.application.exception;

import org.springframework.http.HttpStatus;

public enum ApiErrorCode implements ErrorCode {
    INVALID_REQUEST(1000, "Invalid Request", HttpStatus.BAD_REQUEST),
    MANY_RESULT(1001, "To many Result", HttpStatus.INTERNAL_SERVER_ERROR),
    NO_PRICE_FOUND(1002, "Price not found", HttpStatus.NOT_FOUND),
    RUNTIME(5000, "Runtime Exception", HttpStatus.INTERNAL_SERVER_ERROR),
    EXCEPTION(5001, "Exception", HttpStatus.INTERNAL_SERVER_ERROR);

    private final Integer code;
    private final String message;
    private final HttpStatus httpStatus;

    ApiErrorCode(Integer code, String message, HttpStatus httpStatus) {
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
