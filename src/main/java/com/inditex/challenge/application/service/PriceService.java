package com.inditex.challenge.application.service;

import com.inditex.challenge.application.dto.PriceRequestDto;
import com.inditex.challenge.application.dto.PriceResponseDto;

public interface PriceService {

    PriceResponseDto findPriceByFilter(PriceRequestDto priceRequestDto);
}
