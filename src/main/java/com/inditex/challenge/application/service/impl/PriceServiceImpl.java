package com.inditex.challenge.application.service.impl;

import com.inditex.challenge.application.dto.PriceRequestDto;
import com.inditex.challenge.application.dto.PriceResponseDto;
import com.inditex.challenge.application.exception.ApiErrorCode;
import com.inditex.challenge.application.exception.BusinessException;
import com.inditex.challenge.application.service.PriceService;
import com.inditex.challenge.application.translator.PriceResponseTranslator;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.port.in.FindPriceUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PriceServiceImpl implements PriceService {
    private final Logger logger = LoggerFactory.getLogger(PriceServiceImpl.class);
    private final FindPriceUseCase findPriceUseCase;
    private final PriceResponseTranslator priceResponseTranslator;

    public PriceServiceImpl(FindPriceUseCase findPriceUseCase, PriceResponseTranslator priceResponseTranslator) {
        this.findPriceUseCase = findPriceUseCase;
        this.priceResponseTranslator = priceResponseTranslator;
    }


    public PriceResponseDto findPriceByFilter(PriceRequestDto priceRequestDto) {

        logger.info("start with params: {}", priceRequestDto);

        List<Price> result = findPriceUseCase.findPriceByFilter(priceRequestDto.applicationDate(), priceRequestDto.productId(), priceRequestDto.brandId());

        validateResult(result);

        PriceResponseDto priceResponseDto = result.stream().map(priceResponseTranslator::toDomainToDto).findFirst().orElseThrow(() -> new BusinessException(ApiErrorCode.NO_PRICE_FOUND));

        logger.info("finish return {}:", priceResponseDto);

        return priceResponseDto;

    }

    private static void validateResult(List<Price> result) {

        if (result.size() >= 2 && result.stream().allMatch(price -> result.stream().findFirst().get().getPriority().equals(price.getPriority()))) {
            throw new BusinessException(ApiErrorCode.MANY_RESULT);
        }
    }

}
