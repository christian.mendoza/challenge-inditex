package com.inditex.challenge.application.translator;

import com.inditex.challenge.application.dto.PriceResponseDto;
import com.inditex.challenge.domain.model.Price;


public class PriceResponseTranslator {

    public PriceResponseDto toDomainToDto(Price price) {
        return new PriceResponseDto(price.getId(),
                price.getProductId(),
                price.getBrand().getId(),
                price.getStartDate(),
                price.getEndDate(),
                price.getPrice(),
                price.getPriceList());
    }
}
