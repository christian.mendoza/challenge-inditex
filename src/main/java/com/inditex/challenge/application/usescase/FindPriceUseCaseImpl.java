package com.inditex.challenge.application.usescase;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.port.in.FindPriceUseCase;
import com.inditex.challenge.domain.port.out.PriceRepositoryPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.List;

public class FindPriceUseCaseImpl implements FindPriceUseCase {

    private final Logger logger = LoggerFactory.getLogger(FindPriceUseCaseImpl.class);

    private final PriceRepositoryPort priceRepositoryPort;

    public FindPriceUseCaseImpl(PriceRepositoryPort priceRepositoryPort) {
        this.priceRepositoryPort = priceRepositoryPort;
    }

    @Override
    public List<Price> findPriceByFilter(Instant date, Long productId, Long brandId) {

        logger.info("start params date: {}, productId: {}, brandId: {}", date, productId, brandId);

        List<Price> prices = priceRepositoryPort.findPriceByFilter(date, productId, brandId);

        logger.info("return prices count: {}", prices.size());

        return prices;
    }
}
