package com.inditex.challenge.domain.model;

import java.time.Instant;

public class Brand {
    private final Long id;
    private final String name;
    private Instant createdAt;
    private Instant updatedAt;

    private Brand(Builder builder) {
        id = builder.id;
        name = builder.name;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(Brand copy) {
        Builder builder = new Builder();
        builder.id = copy.getId();
        builder.name = copy.getName();
        return builder;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }


    public static final class Builder {
        private Long id;
        private String name;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }


        public Brand build() {
            return new Brand(this);
        }
    }
}
