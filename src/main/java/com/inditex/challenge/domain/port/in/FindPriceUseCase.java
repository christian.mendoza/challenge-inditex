package com.inditex.challenge.domain.port.in;

import com.inditex.challenge.domain.model.Price;

import java.time.Instant;
import java.util.List;

public interface FindPriceUseCase {

    List<Price> findPriceByFilter(Instant date, Long productId, Long brandId);

}
