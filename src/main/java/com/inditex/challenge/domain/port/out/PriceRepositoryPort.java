package com.inditex.challenge.domain.port.out;

import com.inditex.challenge.domain.model.Price;

import java.time.Instant;
import java.util.List;

public interface PriceRepositoryPort {
    List<Price> findPriceByFilter(Instant date, Long productId, Long brandId);
}
