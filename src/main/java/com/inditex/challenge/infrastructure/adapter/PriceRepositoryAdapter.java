package com.inditex.challenge.infrastructure.adapter;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.port.out.PriceRepositoryPort;
import com.inditex.challenge.infrastructure.entity.PriceEntity;
import com.inditex.challenge.infrastructure.repository.PriceRepositoryJpa;
import com.inditex.challenge.infrastructure.translator.PriceEntityTranslator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

import java.time.Instant;
import java.util.List;

public class PriceRepositoryAdapter implements PriceRepositoryPort {

    private final Logger logger = LoggerFactory.getLogger(PriceRepositoryAdapter.class);

    private final PriceRepositoryJpa priceRepositoryJpa;

    private final PriceEntityTranslator priceEntityTranslator;

    public PriceRepositoryAdapter(PriceRepositoryJpa priceRepositoryJpa, PriceEntityTranslator priceEntityTranslator) {
        this.priceRepositoryJpa = priceRepositoryJpa;
        this.priceEntityTranslator = priceEntityTranslator;
    }

    @Override
    @Cacheable(cacheNames = "prices")
    public List<Price> findPriceByFilter(Instant date, Long productId, Long brandId) {

        logger.info("start params date: {}, productId: {}, brandId: {}", date, productId, brandId);

        List<PriceEntity> priceEntityByFilter = priceRepositoryJpa.findPriceByFilter(date, productId, brandId);

        List<Price> prices = priceEntityByFilter.stream().map(priceEntityTranslator::toModelToDomain).toList();

        logger.info("return prices size: {}", prices.size());

        return prices;
    }
}
