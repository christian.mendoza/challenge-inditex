package com.inditex.challenge.infrastructure.config;

import com.inditex.challenge.application.service.PriceService;
import com.inditex.challenge.application.service.impl.PriceServiceImpl;
import com.inditex.challenge.application.translator.PriceResponseTranslator;
import com.inditex.challenge.application.usescase.FindPriceUseCaseImpl;
import com.inditex.challenge.domain.port.in.FindPriceUseCase;
import com.inditex.challenge.domain.port.out.PriceRepositoryPort;
import com.inditex.challenge.infrastructure.adapter.PriceRepositoryAdapter;
import com.inditex.challenge.infrastructure.repository.PriceRepositoryJpa;
import com.inditex.challenge.infrastructure.service.CacheService;
import com.inditex.challenge.infrastructure.translator.BrandEntityTranslator;
import com.inditex.challenge.infrastructure.translator.PriceEntityTranslator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public FindPriceUseCase findPriceUseCase(PriceRepositoryPort priceRepositoryPort) {
        return new FindPriceUseCaseImpl(priceRepositoryPort);
    }

    @Bean
    public PriceRepositoryPort priceRepositoryPort(PriceRepositoryJpa priceRepositoryJpa, PriceEntityTranslator priceEntityTranslator) {
        return new PriceRepositoryAdapter(priceRepositoryJpa, priceEntityTranslator);
    }

    @Bean
    public PriceService priceService(PriceRepositoryPort priceRepositoryPort, FindPriceUseCase findPriceUseCase, PriceResponseTranslator priceResponseTranslator) {
        return new PriceServiceImpl(new FindPriceUseCaseImpl(priceRepositoryPort), priceResponseTranslator);
    }

    @Bean
    public PriceResponseTranslator priceResponseMapper() {
        return new PriceResponseTranslator();
    }

    @Bean
    public CacheService cacheService() {
        return new CacheService();
    }

    @Bean
    public BrandEntityTranslator brandEntityTranslator() {
        return new BrandEntityTranslator();
    }

    @Bean
    public PriceEntityTranslator priceEntityTranslator(BrandEntityTranslator brandEntityTranslator) {
        return new PriceEntityTranslator(brandEntityTranslator);
    }


}
