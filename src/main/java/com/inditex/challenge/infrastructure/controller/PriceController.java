package com.inditex.challenge.infrastructure.controller;

import com.inditex.challenge.application.dto.PriceRequestDto;
import com.inditex.challenge.application.dto.PriceResponseDto;
import com.inditex.challenge.application.service.PriceService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/prices")
@Validated
public class PriceController {

    private final Logger logger = LoggerFactory.getLogger(PriceController.class);

    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping
    public PriceResponseDto findByFilters(@Valid PriceRequestDto priceRequestDto) {

        logger.info("start params {}", priceRequestDto);

        PriceResponseDto priceByFilter = priceService.findPriceByFilter(priceRequestDto);

        logger.info("finish return {}", priceRequestDto);

        return priceByFilter;
    }

}