package com.inditex.challenge.infrastructure.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;

@Entity
@Table(name = "PRICE",
        indexes = {@Index(name = "priceSearchIndex", columnList = "startDate, endDate, productId, brand_id")},
        uniqueConstraints = {@UniqueConstraint(name = "UniqueStartAndDateEndDateAndPriorityAndProductIdAndBrandId"
                , columnNames = {"startDate", "endDate", "priority", "productId", "brand_id"})})
public class PriceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "brand_id")
    private BrandEntity brandEntity;
    @Column(nullable = false)
    private Instant startDate;
    @Column(nullable = false)
    private Instant endDate;
    @Column(nullable = false)
    private Long productId;
    @Column(nullable = false)
    private Long priority;
    @Column(nullable = false)
    private BigDecimal price;
    @Column(nullable = false)
    private Currency currency;

    private Integer priceList;

    @CreationTimestamp
    private Instant createdAt;

    @UpdateTimestamp
    private Instant updatedAt;

    public PriceEntity() {
    }

    private PriceEntity(Builder builder) {
        id = builder.id;
        brandEntity = builder.brandEntity;
        startDate = builder.startDate;
        endDate = builder.endDate;
        productId = builder.productId;
        priority = builder.priority;
        price = builder.price;
        currency = builder.currency;
        priceList = builder.priceList;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public BrandEntity getBrandEntity() {
        return brandEntity;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public Long getProductId() {
        return productId;
    }

    public Long getPriority() {
        return priority;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Integer getPriceList() {
        return priceList;
    }


    public static final class Builder {
        private Long id;
        private BrandEntity brandEntity;
        private Instant startDate;
        private Instant endDate;
        private Long productId;
        private Long priority;
        private BigDecimal price;
        private Currency currency;
        private Integer priceList;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withBrandEntity(BrandEntity val) {
            brandEntity = val;
            return this;
        }

        public Builder withStartDate(Instant val) {
            startDate = val;
            return this;
        }

        public Builder withEndDate(Instant val) {
            endDate = val;
            return this;
        }

        public Builder withProductId(Long val) {
            productId = val;
            return this;
        }

        public Builder withPriority(Long val) {
            priority = val;
            return this;
        }

        public Builder withPrice(BigDecimal val) {
            price = val;
            return this;
        }

        public Builder withCurrency(Currency val) {
            currency = val;
            return this;
        }

        public Builder withPriceList(Integer val) {
            priceList = val;
            return this;
        }

        public PriceEntity build() {
            return new PriceEntity(this);
        }
    }
}
