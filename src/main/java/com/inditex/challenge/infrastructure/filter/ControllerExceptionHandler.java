package com.inditex.challenge.infrastructure.filter;


import com.inditex.challenge.application.dto.ApiErrorDto;
import com.inditex.challenge.application.exception.ApiErrorCode;
import com.inditex.challenge.application.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ControllerExceptionHandler {
    Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler({RuntimeException.class})
    protected ResponseEntity<Object> handleRuntimeException(RuntimeException ex) {
        logger.error("ERROR: " + ex);
        return new ResponseEntity<>(new ApiErrorDto(ApiErrorCode.RUNTIME.getCode(), Collections.singletonList(ex.getMessage()),
                ApiErrorCode.RUNTIME.getHttpStatus().value()), ApiErrorCode.RUNTIME.getHttpStatus());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorDto> handleException(Exception ex) {
        logger.error("ERROR: " + ex);
        return new ResponseEntity<>(new ApiErrorDto(ApiErrorCode.EXCEPTION.getCode(), Collections.singletonList(ex.getMessage()),
                ApiErrorCode.EXCEPTION.getHttpStatus().value()), ApiErrorCode.EXCEPTION.getHttpStatus());
    }

    @ExceptionHandler({BusinessException.class})
    protected ResponseEntity<Object> handleBusinessException(BusinessException ex) {
        logger.error("ERROR: " + ex);
        return new ResponseEntity<>(new ApiErrorDto(ex.getErrorCode().getCode(), Collections.singletonList(ex.getErrorCode().getMessage()), ex.getErrorCode().getHttpStatus().value()),
                ex.getErrorCode().getHttpStatus());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    protected ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        logger.error("ERROR: " + ex);
        List<String> errors = ex.getBindingResult().getFieldErrors()
                .stream().map(FieldError::getDefaultMessage).collect(Collectors.toList());

        return new ResponseEntity<>(new ApiErrorDto(ApiErrorCode.INVALID_REQUEST.getCode(), errors, ApiErrorCode.INVALID_REQUEST.getHttpStatus().value()
        ), ApiErrorCode.INVALID_REQUEST.getHttpStatus());
    }

}