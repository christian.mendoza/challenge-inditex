package com.inditex.challenge.infrastructure.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Component
public class MdcFilter extends OncePerRequestFilter {
    public static final String REQUEST_ID = "requestId";
    public static final String REQUEST_ID_OVERRIDE = "requestId-override";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String uow = String.format("requestId:%s",
                Optional.ofNullable(request.getHeader(REQUEST_ID_OVERRIDE)).orElse(UUID.randomUUID().toString()));

        MDC.put(REQUEST_ID, uow);
        response.setHeader(REQUEST_ID, uow);
        try {
            filterChain.doFilter(request, response);
        } finally {
            MDC.remove(REQUEST_ID);
        }
    }
}
