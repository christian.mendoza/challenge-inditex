package com.inditex.challenge.infrastructure.repository;

import com.inditex.challenge.infrastructure.entity.PriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface PriceRepositoryJpa extends JpaRepository<PriceEntity, Long> {

    @Query("SELECT p FROM PriceEntity p  JOIN p.brandEntity b " +
            "where p.startDate<= ?1 and p.endDate >= ?1 and p.productId= ?2 and b.id= ?3 " +
            "ORDER BY p.priority DESC LIMIT 2")
    List<PriceEntity> findPriceByFilter(Instant date, Long productId, Long brandId);

}