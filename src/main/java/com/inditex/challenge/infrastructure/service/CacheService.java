package com.inditex.challenge.infrastructure.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;

public class CacheService {
    Logger logger = LoggerFactory.getLogger(CacheService.class);

    @CacheEvict(value = "prices", allEntries = true)
    @Scheduled(fixedRateString = "${caching.prices-ttl}")
    public void emptyPricesCache() {

        logger.info("Evict all prices cache");
    }

}
