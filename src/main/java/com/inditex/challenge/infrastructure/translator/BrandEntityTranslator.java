package com.inditex.challenge.infrastructure.translator;

import com.inditex.challenge.domain.model.Brand;
import com.inditex.challenge.infrastructure.entity.BrandEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BrandEntityTranslator {

    private final Logger logger = LoggerFactory.getLogger(BrandEntityTranslator.class);

    public Brand toModelToDomain(BrandEntity brandEntity) {

        logger.info("start translate brandEntityId {}", brandEntity.getId());

        return Brand.newBuilder().withId(brandEntity.getId())
                .withName(brandEntity.getName())
                .build();
    }
}
