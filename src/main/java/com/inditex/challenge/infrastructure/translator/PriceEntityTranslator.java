package com.inditex.challenge.infrastructure.translator;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.infrastructure.entity.PriceEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PriceEntityTranslator {

    private final Logger logger = LoggerFactory.getLogger(PriceEntityTranslator.class);

    private final BrandEntityTranslator brandEntityTranslator;

    public PriceEntityTranslator(BrandEntityTranslator brandEntityTranslator) {
        this.brandEntityTranslator = brandEntityTranslator;
    }

    public Price toModelToDomain(PriceEntity priceEntity) {

        logger.info("start translate priceEntityId {}", priceEntity.getId());

        return Price.newBuilder()
                .withPrice(priceEntity.getPrice())
                .withPriceList(priceEntity.getPriceList())
                .withCurrency(priceEntity.getCurrency())
                .withEndDate(priceEntity.getEndDate())
                .withPriority(priceEntity.getPriority())
                .withProductId(priceEntity.getProductId())
                .withStartDate(priceEntity.getStartDate())
                .withId(priceEntity.getId())
                .withBrand(brandEntityTranslator.toModelToDomain(priceEntity.getBrandEntity()))
                .build();
    }
}
