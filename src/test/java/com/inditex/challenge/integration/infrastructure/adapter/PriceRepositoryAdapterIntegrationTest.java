package com.inditex.challenge.integration.infrastructure.adapter;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.infrastructure.adapter.PriceRepositoryAdapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class PriceRepositoryAdapterIntegrationTest {

    @Autowired
    private PriceRepositoryAdapter priceRepositoryAdapter;

    @Test
    void test_returnOnePrice() {
        List<Price> prices = priceRepositoryAdapter.findPriceByFilter(Instant.parse("2020-06-14T10:00:00Z"), 35455L, 1L);
        Assertions.assertEquals(prices.size(), 1, "To many result");
        Assertions.assertEquals(prices.get(0).getId(), 1L, "Invalid Brand");
        Assertions.assertEquals(prices.get(0).getProductId(), 35455L, "Invalid Product");
        Assertions.assertEquals(prices.get(0).getCurrency().getCurrencyCode(), "EUR", "Invalid CurrencyCode");
        Assertions.assertEquals(prices.get(0).getPriority(), 0L, "Invalid Priority");
        Assertions.assertEquals(prices.get(0).getStartDate(), Instant.parse("2020-06-14T00:00:00Z"), "Invalid StartDate");
        Assertions.assertEquals(prices.get(0).getEndDate(), Instant.parse("2020-12-31T23:59:59Z"), "EndDate");
        Assertions.assertNotNull(prices.get(0).getId(), "Id is null");

    }

    @Test
    void test_returnManyPrice() {
        List<Price> prices = priceRepositoryAdapter.findPriceByFilter(Instant.parse("2021-08-15T16:00:00Z"), 35456L, 1L);

        Assertions.assertEquals(prices.size(), 2, "Incorrect price count");

        Optional<Price> first = prices.stream().filter(price -> price.getPrice().compareTo(new BigDecimal("40.00")) == 0).findFirst();
        Optional<Price> second = prices.stream().filter(price -> price.getPrice().compareTo(new BigDecimal("50.00")) == 0).findFirst();

        Assertions.assertEquals(first.orElseThrow().getId(), 5L, "Invalid Brand");
        Assertions.assertEquals(first.orElseThrow().getPrice(), new BigDecimal("40.00"), "Invalid price");
        Assertions.assertEquals(first.orElseThrow().getProductId(), 35456, "Invalid Product");
        Assertions.assertEquals(first.orElseThrow().getPriceList(), 4, "Invalid PriceList");
        Assertions.assertEquals(first.orElseThrow().getCurrency().getCurrencyCode(), "EUR", "Invalid CurrencyCode");
        Assertions.assertEquals(first.orElseThrow().getPriority(), 1L, "Invalid Priority");
        Assertions.assertEquals(first.orElseThrow().getStartDate(), Instant.parse("2021-06-15T16:00:00Z"), "Invalid StartDate");
        Assertions.assertEquals(first.orElseThrow().getEndDate(), Instant.parse("2021-12-10T23:59:59Z"), "EndDate");
        Assertions.assertNotNull(first.orElseThrow().getId(), "Id is null");

        Assertions.assertEquals(second.orElseThrow().getId(), 6L, "Invalid Brand");
        Assertions.assertEquals(second.orElseThrow().getPrice(), new BigDecimal("50.00"), "Invalid price");
        Assertions.assertEquals(second.orElseThrow().getProductId(), 35456L, "Invalid Product");
        Assertions.assertEquals(second.orElseThrow().getPriceList(), 4, "Invalid PriceList");
        Assertions.assertEquals(second.orElseThrow().getCurrency().getCurrencyCode(), "EUR", "Invalid CurrencyCode");
        Assertions.assertEquals(second.orElseThrow().getPriority(), 1L, "Invalid Priority");
        Assertions.assertEquals(second.orElseThrow().getStartDate(), Instant.parse("2021-06-15T16:00:00Z"), "Invalid StartDate");
        Assertions.assertEquals(second.orElseThrow().getEndDate(), Instant.parse("2021-12-31T23:59:59Z"), "EndDate");
        Assertions.assertNotNull(second.orElseThrow().getId(), "Id is null");
    }
}
