package com.inditex.challenge.integration.infrastructure.repository;

import com.inditex.challenge.infrastructure.entity.PriceEntity;
import com.inditex.challenge.infrastructure.repository.PriceRepositoryJpa;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.util.List;

@DataJpaTest
public class PriceRepositoryIntegrationTest {

    @Autowired
    private PriceRepositoryJpa priceRepository;

    @Test
    void findPriceByFilter_returnDataOk() {
        List<PriceEntity> priceByFilter = priceRepository.findPriceByFilter(Instant.parse("2020-06-14T10:00:00Z"), 35455L, 1L);
        Assertions.assertEquals(priceByFilter.size(), 1, "To many result");
        Assertions.assertEquals(priceByFilter.get(0).getId(), 1L, "Invalid Brand");
        Assertions.assertEquals(priceByFilter.get(0).getProductId(), 35455L, "Invalid Product");
        Assertions.assertEquals(priceByFilter.get(0).getCurrency().getCurrencyCode(), "EUR", "Invalid CurrencyCode");
        Assertions.assertEquals(priceByFilter.get(0).getPriority(), 0L, "Invalid Priority");
        Assertions.assertEquals(priceByFilter.get(0).getStartDate(), Instant.parse("2020-06-14T00:00:00Z"), "Invalid StartDate");
        Assertions.assertEquals(priceByFilter.get(0).getEndDate(), Instant.parse("2020-12-31T23:59:59Z"), "EndDate");
        Assertions.assertEquals(priceByFilter.get(0).getProductId(), 35455L, "Invalid Product");
        Assertions.assertEquals(priceByFilter.get(0).getPriceList(), 1, "Invalid PriceList");
        Assertions.assertNotNull(priceByFilter.get(0).getId(), "Id is null");

    }


    @Test
    void findPriceByFilter_returnEmpty() {
        List<PriceEntity> priceByFilter = priceRepository.findPriceByFilter(Instant.parse("2030-06-14T16:00:00Z"), 35455L, 1L);
        Assertions.assertTrue(priceByFilter.isEmpty(), "Result not is empty");
    }

}
