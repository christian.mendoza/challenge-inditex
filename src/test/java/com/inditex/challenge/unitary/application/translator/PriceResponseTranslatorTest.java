package com.inditex.challenge.unitary.application.translator;

import com.inditex.challenge.application.dto.PriceResponseDto;
import com.inditex.challenge.application.translator.PriceResponseTranslator;
import com.inditex.challenge.domain.model.Brand;
import com.inditex.challenge.domain.model.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;

public class PriceResponseTranslatorTest {

    private final PriceResponseTranslator brandEntityTranslator = new PriceResponseTranslator();

    @Test
    public void test() {

        Brand brand = Brand.newBuilder()
                .withId(23L)
                .withName("Test").build();

        Price price = Price.newBuilder()
                .withPrice(new BigDecimal("20.89"))
                .withBrand(brand)
                .withEndDate(Instant.parse("2020-06-14T10:00:00Z"))
                .withStartDate(Instant.parse("2020-06-12T10:00:00Z"))
                .withCurrency(Currency.getInstance("EUR"))
                .withPriceList(3)
                .withProductId(5L)
                .withId(6L)
                .build();

        PriceResponseDto priceResponseDto = brandEntityTranslator.toDomainToDto(price);

        Assertions.assertEquals(price.getPriceList(), priceResponseDto.priceList());
        Assertions.assertEquals(price.getId(), priceResponseDto.id());
        Assertions.assertEquals(price.getPrice(), priceResponseDto.price());
        Assertions.assertEquals(price.getEndDate(), priceResponseDto.endDate());
        Assertions.assertEquals(price.getStartDate(), priceResponseDto.startDate());
        Assertions.assertEquals(price.getProductId(), priceResponseDto.productId());
        Assertions.assertEquals(price.getBrand().getId(), priceResponseDto.brandId());


    }
}
