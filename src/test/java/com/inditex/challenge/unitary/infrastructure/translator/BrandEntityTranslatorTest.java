package com.inditex.challenge.unitary.infrastructure.translator;


import com.inditex.challenge.domain.model.Brand;
import com.inditex.challenge.infrastructure.entity.BrandEntity;
import com.inditex.challenge.infrastructure.translator.BrandEntityTranslator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BrandEntityTranslatorTest {

    private final BrandEntityTranslator brandEntityTranslator = new BrandEntityTranslator();

    @Test
    public void toModelToDomain_returnOk() {
        BrandEntity brandEntity = BrandEntity.newBuilder()
                .withId(100L)
                .withName("TEST")
                .build();

        Brand brand = brandEntityTranslator.toModelToDomain(brandEntity);

        Assertions.assertEquals(brandEntity.getId(), brand.getId());
        Assertions.assertEquals(brandEntity.getName(), brand.getName());

    }
}
