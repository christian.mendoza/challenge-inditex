package com.inditex.challenge.unitary.infrastructure.translator;

import com.inditex.challenge.domain.model.Brand;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.infrastructure.entity.BrandEntity;
import com.inditex.challenge.infrastructure.entity.PriceEntity;
import com.inditex.challenge.infrastructure.translator.BrandEntityTranslator;
import com.inditex.challenge.infrastructure.translator.PriceEntityTranslator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PriceEntityTranslatorTest {
    @InjectMocks
    private PriceEntityTranslator priceEntityTranslator;

    @Mock
    private BrandEntityTranslator brandEntityTranslator;


    @Test
    public void toModelToDomain_returnOk() {

        BrandEntity brandEntity = BrandEntity.newBuilder()
                .withId(100L)
                .withName("TEST")
                .build();

        Brand brand = Brand.newBuilder()
                .withId(100L)
                .withName("TEST")
                .build();

        PriceEntity priceEntity = PriceEntity.newBuilder()
                .withStartDate(Instant.parse("2020-06-14T10:00:00Z"))
                .withCurrency(Currency.getInstance("EUR"))
                .withEndDate(Instant.parse("2020-06-15T10:00:00Z"))
                .withPrice(new BigDecimal("10.85"))
                .withPriceList(3)
                .withId(4L)
                .withPriority(10L)
                .withProductId(7L)
                .withBrandEntity(brandEntity)
                .build();

        when(brandEntityTranslator.toModelToDomain(brandEntity)).thenReturn(brand);


        Price price = priceEntityTranslator.toModelToDomain(priceEntity);

        Assertions.assertEquals(priceEntity.getStartDate(), price.getStartDate());
        Assertions.assertEquals(priceEntity.getCurrency(), price.getCurrency());
        Assertions.assertEquals(priceEntity.getEndDate(), price.getEndDate());
        Assertions.assertEquals(priceEntity.getPrice(), price.getPrice());
        Assertions.assertEquals(priceEntity.getPriceList(), price.getPriceList());
        Assertions.assertEquals(priceEntity.getId(), price.getId());
        Assertions.assertEquals(priceEntity.getPriority(), price.getPriority());
        Assertions.assertEquals(priceEntity.getProductId(), price.getProductId());
        Assertions.assertEquals(priceEntity.getBrandEntity().getId(), price.getBrand().getId());


    }
}
